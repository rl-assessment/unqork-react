import React from 'react';

import { componentPayload, statePayload } from './utility/payload';
import renderComponents from './utility/renderComponents';

import styled from 'styled-components';

const StyledDiv = styled.div`
  width: 860px;
  margin: auto;
  display: flex;
  flex-direction: column;
`;

const App = () => {
  return (
    <StyledDiv className="componentPayload">
      {/* {render components per componentPayload} */}
      {componentPayload.map(people => renderComponents(people, statePayload))}
    </StyledDiv>
  );
};

export default App;
