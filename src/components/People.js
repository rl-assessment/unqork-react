import React from 'react';

import styled from 'styled-components';
import { styledColor } from './styledColor';

const StyledPeople = styled.div`
  // color: ${styledColor.lightblue};
  // background-color: ${styledColor.dark};
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  // border-radius: 5px;
  // border: 1px solid rgba(184, 196, 194, 0.25);
  // box-shadow: 2px 3px 4px 2px rgba(0, 0, 0, 0.2);
`;

const StyledGroup = styled.div`
  display: flex;
  flex-flow: row wrap;
  float: left;
  align-items: center;
  justify-content: space-around;
  width: 860px;
  margin: auto;
`;

const People = ({ children }) => {
  return (
    <StyledPeople>
      <h3>Listing People Below:</h3>
      <StyledGroup>{children}</StyledGroup>
    </StyledPeople>
  );
};

export default People;
