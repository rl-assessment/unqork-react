import React from 'react';
import People from '../components/People';
import Person from '../components/Person';

// list all exist components
const checkList = [People, Person];

// function take (componentPayload as config, statePayload) as argument
// return React Component by recursive traverse throught the componentPayload
const renderComponents = (config, statePayload) => {
  if (typeof config.component !== undefined) {
    // handle component doesn't exist yet
    if (!checkList.includes(config.component)) {
      return React.createElement(
        () => <div>The Component: {config.component} has not created</div>,
        { key: config.id }
      );
    }

    // component does exist
    return React.createElement(
      config.component,
      {
        id: config.id,
        key: config.id
      },
      // recursive traverse through children components
      config.children
        ? config.children.map(person => renderComponents(person, statePayload))
        : {
            // passing all properties and initial state to children
            ...config.properties,
            iniState: statePayload[config.properties.age.value]
              ? { ...statePayload[config.properties.age.value] }
              : undefined
          }
    );
  }
};

export default renderComponents;
