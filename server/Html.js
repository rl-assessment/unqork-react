import React from 'react';

// html template to generated application strings before sending it to the client
const Html = ({ title, cssStyles, htmlContent, scriptTags }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" type="text/css" href="main.css">
      ${cssStyles}
      <title>${title}</title>
    </head>
    <body>
      <div id="root">${htmlContent}</div>
      ${scriptTags}
    </body>
  </html>
`;

export default Html;
