import { SAVE_STATE, ADD_SCORE } from './types';

export const saveStateToLoad = personState => async dispatch => {
  // initial dispatch to save/load initial state to redux store
  dispatch({ type: SAVE_STATE, payload: personState });
};

export const incrementScoreAction = (key, incrementScore) => async dispatch => {
  // passing key and method to redux reducer to increment score by key
  dispatch({ type: ADD_SCORE, payload: { key, incrementScore } });
};
