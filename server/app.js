import express from 'express';
import path from 'path';
import cors from 'cors';
import router from './ssr';
import favicon from 'serve-favicon';

export const app = express();

// handle cors | parsing request body
app.use(cors());
// serve favicon
app.use(favicon(path.resolve(__dirname, '../images/R.ico')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// serve the bundle.js as a static file
app.use(express.static(path.resolve(__dirname, '../build')));

// server-side render route
app.use('/', router);

// Global error handler
// eslint-disable-next-line no-unused-vars
// app.use((err, req, res, next) => {
//   const defaultErr = {
//     log: 'Express error handler caught unknown middleware error',
//     status: 500,
//     message: { err: 'An error occurred' }
//   };
//   const errorObj = Object.assign({}, defaultErr, { ...err });
//   console.log(errorObj.log);
//   res.status(errorObj.status).send(errorObj.message.err);
// });

export const server = async () => {
  try {
    // await sequelize.sync({ logging: false }).then(() => console.log('sql connected.'));

    const PORT = process.env.PORT || 3000;
    app.listen(PORT, console.log(`Server listen on port ${PORT}`));
  } catch (e) {
    console.error(e);
  }
};
