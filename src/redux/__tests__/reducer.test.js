import peopleReducer from '../reducers/peopleReducer';

describe('peopleReducer', () => {
  let iniState;
  const fakeAction = { type: 'NOT_A_REAL_ACTION' };
  beforeEach(() => {
    iniState = {
      personState: {}
    };
  });

  it('should provide a default state', () => {
    const result = peopleReducer(undefined, fakeAction);
    expect(result).toEqual(iniState);
  });

  it('should return the same state for unrecognized actions', () => {
    const result = peopleReducer(iniState, fakeAction);
    expect(result).toBe(iniState);
  });

  describe('ADD_SCORE ACTION', () => {
    let action;
    let fakeState = {
      personState: { '1': { name: 'fakeName', initialValue: 5 } }
    };
    beforeEach(() => {
      action = {
        type: 'ADD_SCORE',
        payload: {
          key: '1',
          incrementScore: count => ++count
        }
      };
    });

    it('should return a new state object', () => {
      const result = peopleReducer(fakeState, action);
      expect(result).not.toBe(fakeState);
    });

    it('should increment score', () => {
      const result = peopleReducer(fakeState, action);
      expect(result.personState[action.payload.key].initialValue).toBe(6);
    });
  });
});
