import React from 'react';
import { hydrate } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from '../src/redux/store';
import App from '../src/App';
import '../src/index.css';

hydrate(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
