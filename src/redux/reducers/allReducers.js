import { combineReducers } from 'redux';
// import { persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';

import peopleReducer from './peopleReducer';

// persist store can be removed while component re-load initial data on first render
// const persistConfig = {
//   key: 'root',
//   storage,
//   whitelist: ['myPeople']
// };

const rootReducer = combineReducers({ myPeople: peopleReducer });

// export default persistReducer(persistConfig, rootReducer);

export default rootReducer;
