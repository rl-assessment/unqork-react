import React, { useEffect } from 'react';
// import propTypes from 'prop-types';
// import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import {
  saveStateToLoad,
  incrementScoreAction
} from '../redux/actions/peopleActions';

import useToggler from '../hooks/useToggler';

import styled from 'styled-components';
import { styledColor } from './styledColor';

const StyledPerson = styled.div`
  background-color: ${styledColor.lightGrey};
  width: 258px;
  padding: 0px 5px;
  margin: 5px;
  font-size: 16px;
  border-radius: 5px;
  border: 1px solid rgba(184, 196, 194, 0.25);
  box-shadow: 2px 3px 4px 2px rgba(0, 0, 0, 0.2);
  &:hover {
    cursor: pointer;
    box-shadow: rgba(10, 150, 50, 0.25) 5px 10px 20px 5px;
  }
`;

const StyledP = styled.p`
  padding: 10px 10px 0px 10px;
  font-weight: 700;
`;

const Img = styled.img`
  width: 250px;
  height: 250px;
  border-radius: 250px;
  transition: all 0.3s ease-in-out;
  &:hover {
    cursor: pointer;
    box-shadow: rgba(75, 175, 225, 0.22) 5px 10px 20px 5px;
    transform: translate3d(0px, -1px, 0px);
  }
`;

const StyledButton = styled.button`
  font-size: 18px;
  font-weight: 600;
  margin: 10px;
  padding: 8px 16px;
  border-radius: 5px;
  transition: all 0.3s ease-in-out;
  &:hover {
    cursor: pointer;
    box-shadow: rgba(75, 175, 225, 0.22) 5px 10px 20px 5px;
    transform: translate3d(0px, -1px, 0px);
  }
`;

const ColorAddButton = {
  backgroundColor: '#61dafb',
  color: '#282c34',
  border: '2px solid #282c34'
};

const ColorReset = {
  color: '#dc143c',
  border: '2px solid #dc143c'
};

const Person = ({ id, children }) => {
  // grab personState state from redux store by useSelector hook
  const { personState } = useSelector(state => state.myPeople);
  const dispatch = useDispatch(); // useDispatch hook to dispatch actions

  const [reset, toggleButton] = useToggler(false);

  useEffect(() => {
    // initiate dispatch to save/load initial state to redux store
    dispatch(saveStateToLoad({ [children.age.value]: children.iniState }));
  }, [reset]); //re-render when click on reset

  const handleClick = () => {
    // passing key & method to redux store to increment score by key
    dispatch(incrementScoreAction(children.age.value, children.onClick.value));
  };

  // reset score onClick
  const handleReset = () => toggleButton();

  return (
    <StyledPerson>
      <StyledP style={{ fontSize: '24px' }}>
        {children.iniState.name}:{' '}
        {personState[children.age.value]
          ? personState[children.age.value].initialValue
          : 0}
      </StyledP>
      <Img src={children.avatar.value} alt={children.firstName.value} />
      <StyledP>
        Name: {children.firstName.value} {children.lastName.value}
      </StyledP>
      <StyledP>Age: {children.age.value}</StyledP>
      <StyledButton type="button" onClick={handleClick} style={ColorAddButton}>
        Add Score
      </StyledButton>
      <StyledButton type="button" onClick={handleReset} style={ColorReset}>
        Reset Score
      </StyledButton>
    </StyledPerson>
  );
};

export default Person;
