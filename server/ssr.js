import express from 'express';
import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { Provider } from 'react-redux';
import { store } from '../src/redux/store';
import App from '../src/App';
import { ServerStyleSheet } from 'styled-components';
import Html from './Html';

const router = express.Router();

router.get('/', (req, res) => {
  const sheet = new ServerStyleSheet();

  const title = 'unqork-react';

  const htmlContent = renderToString(
    sheet.collectStyles(
      <Provider store={store}>
        <App />
      </Provider>
    )
  );
  const cssStyles = sheet.getStyleTags();

  const scripts = ['vendors~main.js', 'main.js'];

  const scriptTags = renderToStaticMarkup(
    scripts.map(elem => <script src={elem}></script>)
  );

  const html = Html({ title, htmlContent, cssStyles, scriptTags });

  res.status(200).send(html);
});

// return 404 - Not Found page
router.get('*', (req, res) => {
  res.status(404).send(`
    <html>
      <head>
        <style>
          body { font-family: sans-serif; font-size: 15px; }
          h1 { color: #c7c7c7; text-align: center; }
        </style>
        <title>unqork-react</title>
      </head>
      <body>
        <h1>404 - Not Found</h1>
      </body>
    </html>`);
});

export default router;
