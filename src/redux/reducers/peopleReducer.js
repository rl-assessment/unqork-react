import { SAVE_STATE, ADD_SCORE } from '../actions/types';

const initialState = {
  personState: {}
};

const peopleReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_STATE:
      // initiate store
      return {
        ...state,
        personState: { ...state.personState, ...action.payload }
      };

    case ADD_SCORE:
      // destructure payload items
      const { key, incrementScore } = action.payload;
      // get keyObject by key
      const keyObject = { ...state.personState[key] };
      // increment score by the method passing from payload
      keyObject.initialValue = incrementScore(keyObject.initialValue);

      return {
        ...state,
        personState: { ...state.personState, [key]: { ...keyObject } }
      };

    default:
      return state;
  }
};

export default peopleReducer;
