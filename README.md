# Unqork

## React take-home

## Main Logic under src/utility folder | redux implementation under src/redux folder

- payload.js
- renderComponents.js

## to run app, npm install

- \$ npm run dev to run on local
- or npm run prod-win for window (open index.html in browser)
- or npm run prod-mac for mac (open index.html in browser)

## Server-side rendering under server folder, after npm install

- remove build folder if there is one
- \$ npm run prod-ssr to run on localhost:3000

### by Rocky Lin
