import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
// import { PersistGate } from 'redux-persist/integration/react';
import { store } from './redux/store';

import App from './App';
import './index.css';

render(
  <React.StrictMode>
    <Provider store={store}>
      {/* <PersistGate persistor={persistor}></PersistGate> */}
      <App />
      {/* </PersistGate> */}
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
